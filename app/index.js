import angular from 'angular';

import {ServicesModule} from './services/bundle.services';

//Commented code below is direct dependency of the service (For example for widgets usage);
// import {ServicesModule} from './services/SampleService/SampleService';

import {ControllersModule} from './controllers/bundle.controllers';

angular.module('app', [ServicesModule, ControllersModule]);