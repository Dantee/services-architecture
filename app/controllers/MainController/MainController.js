import {ControllersModule} from '../controllers';

class MainController {
	constructor($scope, SampleService) {
		$scope.test = `Printed type from SampleService.js: ${SampleService.type}`;
	}
}

export default angular.module(ControllersModule)
	.controller('MainController', ['$scope', 'SampleService', MainController]);