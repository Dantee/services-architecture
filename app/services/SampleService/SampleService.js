//Importing services module if you will want to include this service directly. {ServicesModule} is the name of services module.
import {ServicesModule} from '../services.js';

//Direct dependencies
import '../DepService/DepService.js';

class SampleService {
	constructor(DepService) {
		this.type = 'Sample service';
		this.DepService = DepService;
	}
}

//Export services module name and service itself.
export {ServicesModule};
export default angular.module(ServicesModule)
	.service('SampleService', ['DepService', SampleService]);
