//Importing services module if you will want to include this service directly. {ServicesModule} is the name of services module.
import {ServicesModule} from '../services.js';

class SeparateService {
	constructor() {
		this.type = 'Dep service';
	}
}

//Export services module name and service itself.
export {ServicesModule};
export default angular.module(ServicesModule)
	.service('SeparateService', [SeparateService]);
