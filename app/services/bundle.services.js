import {ServicesModule} from './services.js';
import './SampleService/SampleService.js';
import './DepService/DepService.js';
import './SeparateService/SeparateService.js';

export {ServicesModule}